

	// Activity: (database: session30)

	//Aggregate to count the total number of items supplied by Red Farms Inc. ($count stage)

	//Aggregate to count the total number of items with price greater than 50. ($count stage)

	//Aggregate to get the average price of all fruits that are onSale per supplier.($group)

	//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)

	//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)


	//save your query/commands in the mongo.js

// RED FARMS ITEMS SUPPLIED
    db.fruits.aggregate([
		{
			$match : {supplier : "Red Farms Inc."}
		},
		{
			$count : "itemsRedFarms"
		}
	])

// Items with greater than 50

db.fruits.aggregate([
	{
		$match : {price : {$gt : 50}}
	},
	{
		$count : "itemsGreaterThan50"
	}
])

// Average of supplies
db.fruits.aggregate([
	{
		$match : {onSale : true}
	},
	{
		$group : {_id : "$supplier", avgPricePerSupplier : {$avg : "$price"}}
	}
])

// highest price per supplier

db.fruits.aggregate([
    {
        $match : {onSale : true}
    },
    {
        $group: {_id : "$supplier", maxPricePerSupplier : {$max : "$price"}}
    }
])

// lowest price per supplier

db.fruits.aggregate([
    {
        $match : {onSale : true}
    },
    {
        $group: {_id : "$supplier", minPricePerSupplier : {$min : "$price"}}
    }
])